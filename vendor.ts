// Polyfills
import "es6-shim";
import "es6-promise";
import "es7-reflect-metadata/dist/browser";

import "zone.js/dist/zone";

// polyfill for ios/safari
import "intl";
import "intl/locale-data/jsonp/en";