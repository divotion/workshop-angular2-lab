
module.exports = function(config){
  var webpackConfig = require('./webpack.config.js');
  
  config.set({

    frameworks: ['jasmine'],
    exclude: [ ],
    files: [
      'vendor.ts',
      { pattern: 'src/**/*.spec.ts', watched: true }
    ],

    preprocessors: { '**/*!(spec).ts': ['webpack'] },

    webpack: {
      resolve: webpackConfig.resolve,
      module: webpackConfig.module,
      tslint: webpackConfig.tslint
    },

    webpackServer: { noInfo: true },

    reporters: ['progress'],
    port: 9876,
    colors: true,

    logLevel: config.LOG_INFO,

    autoWatch: true,

    browsers: [
      'PhantomJS'
    ],

    singleRun: true
  });
};
